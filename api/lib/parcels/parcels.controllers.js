const { httpGet } = require('../helpers');

module.exports = (models) => {
  var parcel = models.Parcel
  return {
    getParcels: async function (request, h) {

      const parsedAddress = parseAddress(request.query.address)
      if(!parsedAddress.number || !parsedAddress.street || !parsedAddress.type){
        throw Boom.badData('Address must include number, street, and type!')
      }
  
      let zip;
      if(parsedAddress.zip)
      {
        zip = parsedAddress.zip;
      } else if (request.query.zip){
        zip = request.query.zip;
      } else {
        throw Boom.badData('No zip in address or zip field!')
      }

      let prefix = '';
      if(parsedAddress.prefix){
        prefix = parsedAddress.prefix + ' ';
      }

      const streetAddress = `${parsedAddress.number} ${prefix}${parsedAddress.street} ${parsedAddress.type}`;

      return parcel.getParcels(streetAddress, zip);
    },
    getParcel: async function (request, h) {

      let parcel = await models.Parcel.findOne({
        attributes: { exclude: ['id'] },
        where: {
          pin: request.params.parcelPin,
        },
      });

      if (parcel === null) {
        return featureCollection();
      }

      const feature = {
        'type': 'Feature',
        'properties': {  // any unique properties we selected in the query (id, pin, etc.) need to be placed under this properties field to access them
          'pin': parcel.pin
        },
        'geometry': {
          'type': parcel.geometry.type,
          'coordinates': parcel.geometry.coordinates
        }
      };

      return featureCollection([feature]);
    },
    getMapParcels: async function (request, h) {
      var coordinates = request.params.coordinates.replace(/_/g, ' ').replace(/,/g, ', ');
      return parcel.getMapParcels(coordinates);
    },
    getMapParcelsFromBounds: async function (request, h) {
      const query = await parcel.getMapParcelsFromBounds(request.query.minLng, request.query.minLat, request.query.maxLng, request.query.maxLat);

      let mapParcels = []

      for (let i = 0; i < query.length; i++) {
        mapParcels.push({
          'type': 'Feature',
          'properties': {
            'pin': query[i].pin
          },
          'geometry': {
            'type': query[i].geometry.type,
            'coordinates': query[i].geometry.coordinates
          }
        })
      }

      return featureCollection(mapParcels);
    },
    getParcelFromCoordinatePair: async function (request, h) {
      const query = await parcel.getParcelFromCoordinatePair(request.query.lat, request.query.lng);

      let parcelFromCoordPair = []

      for (let i = 0; i < query.length; i++) {
        parcelFromCoordPair.push({
          'type': 'Feature',
          'properties': {
            'pin': query[i].pin
          },
          'geometry': {
            'type': query[i].geometry.type,
            'coordinates': query[i].geometry.coordinates
          }
        })
      }

      return featureCollection(parcelFromCoordPair);
    },
    getParcelInfoFromPin: async function (request, h) {
      // whether a string or array, we need to convert to an array
      let parcelPins = request.query.parcelPin;
      if (!Array.isArray(parcelPins)) {
        parcelPins = [parcelPins];
      }

      // need to send parcel pins separated by commas to the wprdc api
      const url = `https://tools.wprdc.org/property-api/v1/parcels/${parcelPins.join(',')}`;

      return httpGet(url);
    },
    getParcelFromPin: async function (request, h) {
      const query = await parcel.getParcelFromPin(request.query.parcelPin);

      let parcelFromPin = []

      for (let i = 0; i < query.length; i++) {
        parcelFromPin.push({
          'type': 'Feature',
          'properties': {
            'pin': query[i].pin
          },
          'geometry': {
            'type': query[i].geometry.type,
            'coordinates': query[i].geometry.coordinates
          }
        })
      }

      return featureCollection(parcelFromPin);
    }
  };

  function featureCollection(features = []) {
    return {
      'type': 'FeatureCollection',
      'features': features
    };
  }

  function parseAddress(address){
    const Parser = require('parse-address');
    const parsedAddress = Parser.parseLocation(address); 

    return parsedAddress;
  }
}
