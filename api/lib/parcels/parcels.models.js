const Joi = require('joi');

const parcelPin = Joi.alternatives().try(
  Joi.string().alphanum().min(16).max(16),
  Joi.string().valid("COMMON GROUND")
);

module.exports = {
  db: (sequelize, Sequelize) => {
    const { Model, Op } = require('sequelize');
    class Parcel extends Model { }

    Parcel.init({
      id: {
        type: Sequelize.INTEGER,
        field: 'ogc_fid',
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
      },
      pin: {
        type: Sequelize.STRING,
        field: 'pin'
      },
      geometry: {
        type: Sequelize.GEOMETRY,
        field: 'wkb_geometry',
      }
    }, {
      modelName: 'Parcel',
      tableName: 'parcels',
      sequelize,
      timestamps: false,  //excludes createdAt and updatedAt fields from query
    });

    console.log('Checking for parcels with invalid geometry...');
    Parcel.findAll({  // check the database for parcels with invalid geometry
      where: sequelize.where(
        sequelize.fn('ST_IsValid',
          sequelize.col('wkb_geometry')),
        'FALSE'),
    }).then(invalidParcels => {
      if (invalidParcels.length === 0) {
        console.log('No parcels with invalid geometry!');
      }
      else {  // if there are parcels with invalid geometry, log them and validate the geometry
        console.log(`There are ${invalidParcels.length} parcels with invalid geometry. Their ID and PIN's are:`);

        const clause = [];

        invalidParcels.forEach(parcel => {
          console.log(`ID: ${parcel.id} - PIN: ${parcel.pin}`);
          clause.push({ id: parcel.id });
        });

        // make the geometry valid for the parcels that failed the validity check
        Parcel.update({
          //ST_CollectionExtract will turn a GeometryCollection into an appropriate geometry type, and won't affect geometries that aren't a GeometryCollection
          geometry: sequelize.fn('ST_CollectionExtract', sequelize.fn('ST_MakeValid', sequelize.col('wkb_geometry')))
        }, {
          where: { [Op.or]: clause }
        }).then(result => {
          console.log(`${result[0]} parcels had their geometry validated.`);
        }).catch(error => {
          console.log(`Couldn't validate geometry. ${error}`)
        });
      }
    }).catch(error => {
      console.log(`Error finding parcels!. ${error}`)
    });

    //builds an array of parcels that cover the address or addresses received from getParcels
    Parcel.getParcelList = async (addresses) => {
      let results_array = []
      await Promise.all(addresses.map(async (address) => {
        //Pulls the SRID from the retrieved address and strips formatting so it can be used in the query
        //TO DO: Consider adding a default SRID in case this formatting fails to work
        var SRID = address.geometry.crs.properties.name.replace("EPSG:", "");
        await Parcel.findAll({
          attributes: { exclude: ['id', 'geometry'] },
          where: sequelize.where(
            sequelize.fn('ST_Covers',
              sequelize.col('wkb_geometry'),
              sequelize.fn('ST_GeomFromText', address.get('geotext'), SRID)
            ),
            'TRUE'),
        }).then(parcels => {
          parcels.forEach(parcel => {
            results_array.push(parcel)
          });
          return;
        });
      }))
      return results_array;
    };

    //searches for addresses, then sends them to getParcelList to get the array of parcels it will return
    Parcel.getParcels = async (address, zip) => {
      const parcel_list = await sequelize.models.Address.findAll({
        attributes: {
          include: [[sequelize.fn('ST_AsText', sequelize.col('wkb_geometry')), 'geotext']]
        },
        where: {
          [Op.and]: [
            {
              full_address: {
                [Op.iLike]: address
              }
            },
            {
              zip_code: {
                [Op.iLike]: zip
              }
            }
          ]
        }
      }).then(addresses => {
        if (addresses.length == 0) {
          return addresses
        }
        else {
          return Parcel.getParcelList(addresses)
        };
      });
      return parcel_list;
    };

    Parcel.getMapParcels = async (coordinates) => {
      let mapParcels = await Parcel.findAll({
        attributes: { exclude: ['id'] },
        where: sequelize.where(
          sequelize.fn(
            'ST_Intersects',
            sequelize.fn('ST_GeomFromText', `POLYGON((${coordinates}))`, '4326'),
            sequelize.col('wkb_geometry')
          ),
          'TRUE')
      });
      return mapParcels;
    };

    Parcel.getMapParcelsFromBounds = async (minLat, minLng, maxLat, maxLng) => {
      let mapParcels = await Parcel.findAll({
        attributes: { exclude: ['id'] },
        where: sequelize.where(
          sequelize.fn(
            'ST_Intersects',
            sequelize.fn('ST_GeomFromText', `POLYGON((${minLat} ${minLng}, ${minLat} ${maxLng}, ${maxLat} ${maxLng}, ${maxLat} ${minLng}, ${minLat} ${minLng}))`, '4326'),
            sequelize.col('wkb_geometry')
          ),
          'TRUE')
      });
      return mapParcels;
    };

    Parcel.getParcelFromCoordinatePair = async (lat, lng) => {
      let parcel = await Parcel.findAll({
        attributes: { exclude: ['id'] },
        where: sequelize.where(
          sequelize.fn(
            'ST_Intersects',
            sequelize.fn('ST_GeomFromText', `POINT(${lng} ${lat})`, '4326'),
            sequelize.col('wkb_geometry')
          ),
          'TRUE')
      });
      return parcel;
    };

    Parcel.getParcelFromPin = async (parcelPins) => {
      let parcelPinsArray = [parcelPins];

      const clause = [];

      parcelPinsArray.forEach(pin => {
        clause.push({ pin: pin },)
      });

      let parcel = await Parcel.findAll({
        where: {
          [Op.or]: clause
        }
      });
      return parcel;
    };

    return Parcel;
  },
  addressLookup: Joi.object().keys({
    address: Joi.string().required(),
    zip: Joi.string().optional().allow(null, ''),
  }),
  parcelPin: parcelPin,
  parcelPinLookup: Joi.object().keys({
    parcelPin: Joi.alternatives().try(
      Joi.array().items(parcelPin),
      parcelPin,
    ).required().description('The 16 character parcel id number (or "COMMON GROUND") either as a single string or an array of strings')
  })
}
