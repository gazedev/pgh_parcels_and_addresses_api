const Joi = require('joi');
const parcelsModels = require('./parcels.models');

module.exports = {
  routes: (models) => {
    const controllers = require('./parcels.controllers')(models);
    return [
      {
        path: '/parcels',
        method: 'GET',
        handler: controllers.getParcels,
        options: {
          description: 'Get an array of parcels that contain the specified address',
          notes: 'This endpoint searches for addresses matching a given full_address (i.e. 311 Washington Blvd) and five-digit zip-code then finds any parcels that contain the returned address or addresses.',
          tags: ['api', 'Addresses', 'Parcels'],
          validate: {
            query: parcelsModels.addressLookup,
          },
        }
      },
      {
        path: '/parcels/{parcelPin}',
        method: 'GET',
        handler: controllers.getParcel,
        options: {
          description: 'Get a single parcel by its parcelId',
          notes: 'This endpoint searches for addresses matching a given full_address (i.e. 311 Washington Blvd) and five-digit zip-code then finds any parcels that contain the returned address or addresses.',
          tags: ['api', 'Parcels'],
          validate: {
            params: Joi.object().keys({
              parcelPin: parcelsModels.parcelPin.required().description('The 16 character parcel id number (or "COMMON GROUND")'),
            })
          }
        }
      },
      {
        path: '/parcels/info',
        method: 'GET',
        handler: controllers.getParcelInfoFromPin,
        options: {
          description: 'Returns info about parcels based on the parcel IDs passed as parameters',
          notes: `Parcel IDs are passed as a parameter 'parcelPin' and multiple can be passed in`,
          tags: ['api', 'Parcels'],
          validate: {
            query: parcelsModels.parcelPinLookup
          }
        }
      },
      {
        path: `/mapparcels/{coordinates}`,
        method: 'GET',
        handler: controllers.getMapParcels,
        options: {
          description: 'Get an array of parcels that is currently within the bounds of the map',
          notes: 'This endpoint searches for parcels that are contained within the boundaries of the React app map',
          tags: ['api', 'Parcels'],
        }
      },
      {
        path: `/parcels/bounds`,
        method: 'GET',
        handler: controllers.getMapParcelsFromBounds,
        options: {
          description: 'Get an array of parcels that is currently within the bounds of the map',
          notes: 'This endpoint searches for parcels that are contained within the boundaries of the React app map',
          tags: ['api', 'Parcels'],
          validate: {
            query: Joi.object().keys({
              minLat: Joi.number().min(-90).max(90).required().less(Joi.ref('maxLat')),
              maxLat: Joi.number().min(-90).max(90).required(),
              minLng: Joi.number().min(-180).max(180).required().less(Joi.ref('maxLng')),
              maxLng: Joi.number().min(-180).max(180).required(),
            })
          }
        }
      },
      {
        path: `/parcels/coordinatepair`,
        method: 'GET',
        handler: controllers.getParcelFromCoordinatePair,
        options: {
          description: 'Get a single parcel based on a coordinate pair',
          notes: 'This endpoint searches the parcel that contains a single coordinate pair',
          tags: ['api', 'Parcels'],
          validate: {
            query: Joi.object().keys({
              lat: Joi.number().min(-90).max(90).required(),
              lng: Joi.number().min(-180).max(180).required()
            })
          }
        }
      },
      {
        path: `/parcels/pin`,
        method: 'GET',
        handler: controllers.getParcelFromPin,
        options: {
          description: 'Returns parcels based on the parcel IDs passed as parameters',
          notes: `Parcel IDs are passed as a parameter 'parcelPin' and multiple can be passed in`,
          tags: ['api', 'Parcels'],
          validate: {
            query: parcelsModels.parcelPinLookup
          }
        }
      }
    ];
  }
};
