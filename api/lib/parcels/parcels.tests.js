const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const Assert = require('assert');
const lab = exports.lab = Lab.script();
// const Lab = require('lab');
let server;

lab.before(async () => {
  const index = await require('../../index.js');
  server = await index.server;
  await index.sequelize;
});

lab.test('One Address may have multiple parcel ids - parcel.test.js', { timeout: 10000 }, async () => {
  // wait for the response and the request to finish
  const response = await server.inject({
    method: 'GET',
    url: '/parcels?address=301+5th+Ave&zip=15222'
  });

  const payload = JSON.parse(response.payload);

  expect(payload).to.include([{ pin: '0001D00259000000' }, { pin: '0001D00259C71700' }, { pin: '0001D00259000A00' }, { pin: '0001D00259000C00' }]);
});

lab.test('One address that has one parcel id - parcel.test.js', { timeout: 10000 }, async () => {
  // wait for the response and the request to finish
  const response = await server.inject({
    method: 'GET',
    url: '/parcels?address=221+Provost+Rd&zip=15227'
  });

  const payload = JSON.parse(response.payload);

  // TODO: make the matching less picky
  expect(payload).to.include([{ pin: '0138P00115000000' }]);
});

lab.test('An address may not have a parcel ID - parcel.test.js', { timeout: 10000 }, async () => {
  // wait for the response and the request to finish
  const response = await server.inject({
    method: 'GET',
    url: '/parcels?address=0+Example+Rd&zip=99999'
  });

  const payload = JSON.parse(response.payload);

  //console.log(response)
  expect(payload).to.be.empty();
});

