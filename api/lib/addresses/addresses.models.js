const Joi = require('joi');

module.exports = {
  db: (sequelize, Sequelize) => {
    const { Model, Op } = require('sequelize');
    class Address extends Model { }

    Address.init({
      id: {
        type: Sequelize.INTEGER,
        field: 'ogc_fid',
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
      },
      addr_num: {
        type: Sequelize.STRING,
        field: 'addr_num'
      },
      st_prefix: {
        type: Sequelize.STRING,
        field: 'st_prefix'
      },
      st_name: {
        type: Sequelize.STRING,
        field: 'st_name'
      },
      st_type: {
        type: Sequelize.STRING,
        field: 'st_type'
      },
      municipality: {
        type: Sequelize.STRING,
        field: 'municipali'
      },
      state: {
        type: Sequelize.STRING,
        field: 'state'
      },
      zip_code: {
        type: Sequelize.STRING,
        field: 'zip_code'
      },
      full_address: {
        type: Sequelize.STRING,
        field: 'full_addre'
      },
      geometry: {
        type: Sequelize.GEOMETRY,
        field: 'wkb_geometry',
      }
    }, {
      modelName: 'Address',
      tableName: 'addresses',
      sequelize,
      timestamps: false,  //excludes createdAt and updatedAt fields from query
    });

    Address.getAddressList = async (parcels) => {
      let results_array = []
      await Promise.all(parcels.map(async (parcel) => {
        //Pulls the SRID from the retrieved address and strips formatting so it can be used in the query
        var SRID = parcel.geometry.crs.properties.name.replace("EPSG:", "");
        await Address.findAll({
          where: sequelize.where(sequelize.fn('ST_CoveredBy', sequelize.col('wkb_geometry'), sequelize.fn('ST_GeomFromText', parcel.get('geotext'), SRID)), 'TRUE'),
        }).then(addresses => {
          addresses.forEach(address => {
            results_array.push(address);
          });
          return;
        });
      }))
      return results_array;
    };

    Address.getAddresses = async (parcelID) => {
      const srid = 4326;
      const addressList = await sequelize.models.Parcel.findAll({
        attributes: {
          include: [[sequelize.fn('ST_AsText', sequelize.col('wkb_geometry')), 'geotext']]
        },
        where: {
          pin: parcelID
        }
      }).then(parcels => {
        if (parcels.length == 0) {
          return parcels
        }
        else {
          return Address.getAddressList(parcels)
        };
      });
      return addressList;
    };

    Address.getAddressesFromBounds = async (minLat, minLng, maxLat, maxLng) => {
      let mapAddresses = await Address.findAll({
        attributes: { exclude: ['id'] },
        where: sequelize.where(
          sequelize.fn(
            'ST_Covers',
            sequelize.fn('ST_GeomFromText', `POLYGON((${minLat} ${minLng}, ${minLat} ${maxLng}, ${maxLat} ${maxLng}, ${maxLat} ${minLng}, ${minLat} ${minLng}))`, '4326'),
            sequelize.col('wkb_geometry')
          ),
          'TRUE')
      });
      return mapAddresses;
    };

    Address.getAddressesFromPin = async (parcelPins) => {
      let parcelPinsArray = [parcelPins];

      const clause = [];

      parcelPinsArray.forEach(pin => {
        clause.push({ pin: pin },)
      });

      const addressList = await sequelize.models.Parcel.findAll({
        attributes: {
          include: [[sequelize.fn('ST_AsText', sequelize.col('wkb_geometry')), 'geotext']]
        },
        where: {
          [Op.or]: clause
        }
      }).then(parcels => {
        if (parcels.length == 0) {
          return parcels
        }
        else {
          return Address.getAddressList(parcels)
        };
      });
      return addressList;
    };

    return Address;
  },
  parcelId: Joi.object().keys({
    parcelId: Joi.string().required(),
  }),
  addressSearch: Joi.object().keys({
    q: Joi.string().required(),
  }),
}
