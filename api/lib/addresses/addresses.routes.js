const Joi = require('joi');
const addressesModels = require('./addresses.models');
const parcelsModels = require('../parcels/parcels.models');

module.exports = {
  routes: (models) => {
    const controllers = require('./addresses.controllers')(models);
    return [
      {
        path: '/addresses/{parcelId}',
        method: 'GET',
        handler: controllers.getAddresses,
        options: {
          description: 'Get an array of addresses located within the specified parcel',
          notes: 'This currently assumes that parcel IDs are unique for all intents and purposes, or at least that if there is more than one object on the table sharing the identical parcel ID, it will also share the same geographic location. It also hardcodes the SRID. This might be potentially pulled out from the table instead. For more info on SRIDs see https://postgis.net/workshops/postgis-intro/projection.html',
          tags: ['api', 'Addresses', 'Parcels'],
          validate: {
            params: addressesModels.parcelId,
          }
        }
      },
      {
        path: `/addresses/bounds`,
        method: 'GET',
        handler: controllers.getAddressesFromBounds,
        options: {
          description: 'Get an array of addresses that is currently within the bounds of the map',
          notes: 'This endpoint searches for addresses that are contained within the boundaries of the React app map',
          tags: ['api', 'Addresses'],
          validate: {
            query: Joi.object().keys({
              minLat: Joi.number().min(-90).max(90).required().less(Joi.ref('maxLat')),
              maxLat: Joi.number().min(-90).max(90).required(),
              minLng: Joi.number().min(-180).max(180).required().less(Joi.ref('maxLng')),
              maxLng: Joi.number().min(-180).max(180).required(),
            })
          }
        }
      },
      {
        path: `/addresses/pin`,
        method: 'GET',
        handler: controllers.getAddressesFromPin,
        options: {
          description: 'Get addresses based on parcel IDs passed in as paramters in the URL',
          notes: 'This endpoint searches the addresses contained by the parcel IDs passed into the URL',
          tags: ['api', 'Addresses', 'Parcels'],
          validate: {
            query: parcelsModels.parcelPinLookup
          }
        }
      },
      {
        path: `/addresses/search`,
        method: 'GET',
        handler: controllers.addressSearch,
        options: {
          description: 'Return potential addresses from a search query',
          notes: 'This endpoint searches the addresses table for addresses matching the q query param',
          tags: ['api', 'Addresses'],
          validate: {
            query: addressesModels.addressSearch
          }
        }
      }
    ]
  }
}
