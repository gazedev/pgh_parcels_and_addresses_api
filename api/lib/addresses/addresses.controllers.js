module.exports = (models) => {
  const Sequelize = require('sequelize');
  const Op = Sequelize.Op;
  var address = models.Address;
  return {
    getAddresses: async function (request, h) {
      return address.getAddresses(request.params.parcelId);
    },
    getAddressesFromBounds: async function (request, h) {
      const query = await address.getAddressesFromBounds(request.query.minLng, request.query.minLat, request.query.maxLng, request.query.maxLat);

      let mapAddresses = []

      for (let i = 0; i < query.length; i++) {
        mapAddresses.push({
          'type': 'Feature',
          'properties': {  // included majority of the info we query for now, but this can be modified later
            'addr_num': query[i].addr_num,
            'st_name': query[i].st_name,
            'st_type': query[i].st_type,
            'municipality': query[i].municipality,
            'state': query[i].state,
            'zip_code': query[i].zip_code,
            'full_address': query[i].full_address
          },
          'geometry': {
            'type': query[i].geometry.type,
            'coordinates': query[i].geometry.coordinates
          }
        })
      }
      return featureCollection(mapAddresses);
    },
    getAddressesFromPin: async function (request, h) {
      const query = await address.getAddressesFromPin(request.query.parcelPin);
      let addressesFromPin = []

      for (let i = 0; i < query.length; i++) {
        addressesFromPin.push({
          'type': 'Feature',
          'properties': {  // included majority of the info we query for now, but this can be modified later
            'addr_num': query[i].addr_num,
            'st_name': query[i].st_name,
            'st_type': query[i].st_type,
            'municipality': query[i].municipality,
            'state': query[i].state,
            'zip_code': query[i].zip_code,
            'full_address': query[i].full_address
          },
          'geometry': {
            'type': query[i].geometry.type,
            'coordinates': query[i].geometry.coordinates
          }
        })
      }

      return featureCollection(addressesFromPin);
    },
    addressSearch: async function (request, h) {
      const query = request.query.q;
      const where = {
        full_address: {
          [Op.iLike]: query + '%',
        }
      };
      // We want to set a limit so the process doesn't run out of memory on queries with too many matches
      const limit = 1000;
      const results = await models.Address.findAll({
        where,
        limit,
      });
      return results;
    },
  };

  function featureCollection(features = []) {
    return {
      'type': 'FeatureCollection',
      'features': features
    };
  }
}
