# Node
FROM node:14.16.1

# Create app directory
WORKDIR /usr/src/api

COPY ./wait-for-it ../wait-for-it

RUN npm install -g nodemon

EXPOSE 8081
