# Parcel and Addresses API

This is an attempt to load the geo-json of allegheny county parcels, address
points, and other notable shape data (such as pittsburgh neighborhoods) into a
postgis database that can output geojson data, and possibly act as a local map
vector tiles server.

Address points:
- https://data.wprdc.org/dataset/allegheny-county-addressing-address-points2
- https://data.wprdc.org/dataset/4988ae5c-a677-4a7f-9bd0-e735c19a8ff3/resource/492a62fb-0560-43f0-9f8e-037fc51343a0/download/allegheny_county__addressing_address_points.geojson

Parcel boundaries:
- https://data.wprdc.org/dataset/allegheny-county-parcel-boundaries1
- https://data.wprdc.org/dataset/709e4e52-6f82-4cd0-a848-f3e2b3f5d22b/resource/3f50d47a-ab54-4da2-9f03-8519006e9fc9/download/allegheny_county_parcel_boundaries.geojson

Pittsburgh neighborhoods:
- https://data.wprdc.org/dataset/neighborhoods2
- https://pghgishub-pittsburghpa.opendata.arcgis.com/datasets/dbd133a206cc4a3aa915cb28baa60fd4_0.geojson


## Running the system locally

The expectation is that you have `docker` and `docker-compose` installed 
and that you are running on a unix-y terminal/system (Linux, Mac, WSL).

Initialize variables.env:
`cp variables.example.env variables.env`

To download datasets, prepare them, and move them to be imported when the database starts:
`./clear-and-reload-data.sh`

To start the database and api:
`docker-compose up postgis api`

## Testing
Initialize testing variables:
`cp variables-test-example.env variables-test.env`

Run tests:
`docker-compose --file=docker-compose-test.yml up`

The tests that will be run are the ones at `lib/{module}/{module}.tests.js`
