#!/bin/sh

# To be run in the postgis container, after postgis setup


# import addresses
psql -U $POSTGRES_USER -d $POSTGRES_DB -f /var/lib/postgresql/imports/addresses.sql

# import parcels
psql -U $POSTGRES_USER -d $POSTGRES_DB -f /var/lib/postgresql/imports/parcels.sql

# import neighborhoods
psql -U $POSTGRES_USER -d $POSTGRES_DB -f /var/lib/postgresql/imports/neighborhoods.sql
